package main

import "fmt"
import "io/ioutil"
import "os"
import "strings"
import "archive/zip"

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Param missing")
        os.Exit(1)
    }
    jarFile := os.Args[1]
    r, err := zip.OpenReader(jarFile)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    defer r.Close()
    for _, f := range r.File {
        if strings.HasSuffix(f.Name, "pom.properties") {
            fr,err := f.Open()
            if err != nil {
                fmt.Println(err)
                os.Exit(1)
            }
            defer fr.Close()
            content, err := ioutil.ReadAll(fr)
            if err != nil {
                fmt.Println(err)
                os.Exit(1)
            }
            fmt.Print(string(content))
            os.Exit(0)
        }
    }
    fmt.Println("Can not find pom.properties")
    os.Exit(1)
}
